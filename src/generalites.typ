#let generalites = [
== Présentation générale

Ce cours du $5^e$ bloc a pour but d'initier l'étudiant à la collaboration 
en équipe de développement d'applications et à la méthodologie de 
développement. Il fait suite au cours portant sur le développement d'objets 
intelligents et prépare l'étudiant au projet intégrateur.



À l'issue de ce cours, l'étudiant sera en mesure de collaborer avec les 
différents acteurs d'une équipe de développement lors d'un projet commun
lié à la conception d'une application, en respectant les normes de 
développement. Les objectifs intermédiaires de ce cours sont de participer
à l'élaboration du cahier des charges fonctionnel, d'utiliser une
méthodologie de développement pour collaborer en équipe, de modéliser des 
applications ainsi que de documenter le processus de développement.


Les principaux thèmes abordés dans ce cours sont: l'élaboration du cahier de 
charge fonctionnel; la conception générale et détaillée; les méthodes Agile 
appliquées au développement logiciel ainsi que les outils de collaboration 
et de gestion de versions. 
]