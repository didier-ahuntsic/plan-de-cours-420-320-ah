#let _ponderation = link("https://www.cegepsquebec.ca/nos-cegeps/presentation/systeme-scolaire-quebecois/grille-de-cours-et-ponderation/")[Pondération]

#let frontpage = [ 
#align(center)[
    #image("logo-ahuntsic.png", width: 33%),

    #text(size: 20pt)[Plan De Cours]

    #text(size: 12pt)[Automn 2023]
]

#table(
  columns: (auto, 1fr),
  inset: 25pt,
  align: horizon,
  [Titre du cours],[*Collaboration à la conception d'applications*],
  [Code],[*420-320-AH*],
  _ponderation, text(weight: "bold", font: "FreeMono")[1-2-2],
  [Compétences visées], list(
        [*AF39 - Effectuer le développement d'applications natives avec une base de donnée.*],
    ),
  [Préalable], [
    - 420-315-AH - Développement d'objets intelligents
  ],
  [Unités], [1.66],
  [Corequis], [Aucun],
  [Enseignant], [
    - *Didier Amyot*
    - #link("mailto:didier.amyot@collegeahuntsic.qc.ca")[didier.amyot\@collegeahuntsic.qc.ca]
    ],
  [Département], [*Informatique*]
)
]

