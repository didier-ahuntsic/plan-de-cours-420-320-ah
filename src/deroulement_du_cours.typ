#let section-counter = counter("section-counter")
#let course-counter = counter("course-counter")

#let homework-counter = counter("homework-counter")


#let section(content) = {
    section-counter.step()
    let idx = section-counter.display();
    heading(level: 3)[ Étape #idx: #content ];
}

#let course-number() = [
  #course-counter.step()
  #course-counter.display()
]

#let homework-number() = [
  #homework-counter.step()
  #homework-counter.display()
]

#let intro = [

#section[Introduction à la conception d'applications]

==== Objectifs spécifiques

À l'issue de cette étape, l'élève sera en mesure concevoir
des diagrammes UML en utilisant  des logiciels de conception.

==== Contenu et programmation 
#table(
  columns: (auto, 1fr),
  inset: 10pt,
  align: horizon,
  [*Cours*], [*Contenu*], 
  course-number(), [Méthodes de développement], 
  course-number(), [Cycle de vie et cas d'utilisation],
  course-number(), [Diagramme de séquence],
)

==== Méthodologie
L'élève devra faire des lectures qui lui permettront de réaliser les activités d'apprentissage.

==== Activités d'apprentissage 

Exercices à faire à la maison et en classe.


==== Évaluation

Une évaluation pratique (Travail Pratique #homework-number()) à faire après l'étape portera sur les différents diagrammes.

]

#let conception-gerenale = [

#section[ Conception générale et détaillée]

==== Objectifs spécifiques

À l'issue de cette étape, l'étudiant sera en mesure concevoir d'autres diagrammes UML en 
utilisant des logiciels de conception. Il sera aussi capable d'effectuer des tests unitaires.


==== Contenu et programmation 
#table(
  columns: (auto, 1fr),
  inset: 10pt,
  align: horizon,
  [*Cours*], [*Contenu*], 
  course-number(), [Modèles d'architectures logicielles], 
  course-number(), [Tests unitaires et débogage],
  course-number(), [Modèle objet et relationnel],
  course-number(), [Patrons de conception],
  course-number(), [Patrons de conception],
)

==== Méthodologie
L'élève devra faire des lectures qui lui permettront de réaliser les activités d'apprentissage.

==== Activités d'apprentissage 

Exercices à faire à la maison et en classe.


==== Évaluation

Une évaluation pratique (Travail Pratique #homework-number()) à faire après l'étape portera sur les tests.
]


#let methode-agile = [

#section[ Conception générale et détaillée]

==== Objectifs spécifiques

À l'issue de cette étape, l'élève sera capable de reconnaître les différents aspects des 
méthodes Agiles. Il sera aussi en mesure d'effectuer de la restructuration de code.


==== Contenu et programmation 
#table(
  columns: (auto, 1fr),
  inset: 10pt,
  align: horizon,
  [*Cours*], [*Contenu*], 
  course-number(), [Méthode Agile], 
  course-number(), [Rétroaction et rétrospective],
  course-number(), [Assurance qualité et restructuration],
)

==== Méthodologie
L'élève devra faire des lectures qui lui permettront de réaliser les activités d'apprentissage.

==== Activités d'apprentissage 

Exercices à faire à la maison et en classe.


==== Évaluation

Une évaluation pratique (Travail Pratique #homework-number()) à faire après l'étape portera sur la restructuration de code.

]

#let agile = [


#section[ Gestion de version]

==== Méthode Agile

À l'issue de cette étape, l’élève sera capable de reconnaître les différents aspects des
méthodes Agiles. Il sera aussi en mesure d’effectuer de la restructuration de code.

==== Contenu et programmation
#table(
  columns: (auto, 1fr),
  inset: 10pt,
  align: horizon,
  [*Cours*], [*Contenu*],
  course-number(), [Méthode Agile],
  course-number(), [Assurance qualité et restructuration],
)


==== Méthodologie
L’élève devra faire des lectures qui lui permettront de réaliser les activités d’apprentissage.

==== Activités d’apprentissage
Exercices à faire à la maison et en classe.

==== Évaluation
Une évaluation pratique (Travail Pratique #homework-number()) à faire après l'étape portera sur la restructuration de code.

]


#let git = [


#section[ Gestion de version]

==== Objectifs spécifiques

À l'issue de cette étape, l'élève sera capable e travailler avec les outils de collaboration et de gestion de versions.


==== Contenu et programmation 
#table(
  columns: (auto, 1fr),
  inset: 10pt,
  align: horizon,
  [*Cours*], [*Contenu*], 
  course-number(), [Introduction à Git], 
  course-number(), [Commandes principales],
  course-number(), [Gestion des branches],
)

==== Méthodologie
L'élève devra faire des lectures qui lui permettront de réaliser les activités d'apprentissage.

==== Activités d'apprentissage 

Exercices à faire à la maison et en classe.


==== Évaluation

Une évaluation pratique (Travail Pratique #homework-number()) à faire après l'étape portera sur Git
et la gestion de version.

]

#let examen-mi-session = [
#section[Examen Mi-Session]

Évaluation portera sur l’ensemble de la matière vue.

==== Contenu et programmation
#table(
  columns: (auto, 1fr),
  inset: 10pt,
  align: horizon,
  [*Cours*], [*Contenu*],
  course-number(), [Examen mi-session],
)
]

#let examen-final = [
#section[Examen Final]

Évaluation portera sur l’ensemble de la matière vue.

==== Contenu et programmation
#table(
  columns: (auto, 1fr),
  inset: 10pt,
  align: horizon,
  [*Cours*], [*Contenu*],
  course-number(), [Examen Final],
)
]

#let deroulement-du-cours = [


#git
#intro

#examen-mi-session
#agile
#conception-gerenale



#examen-final

]
