#let course-bibliography = [
// Bellow is a little hack to display bibliography 
// whitout using them in the text.  Indeed, we use
// them in a text but we set the size to 0.
#text(size: 0em)[
    @mozilla
    @deno_manual
    @nodejs_doc
    @typescript
    @Javascript_pour_tous
]



#bibliography("bibliography.yaml")


]